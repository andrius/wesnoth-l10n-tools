#!/bin/bash
# Pack translations
# Copyright (C) 2007-2008  Andrius Štikonas <andrius@stikonas.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

source "$(dirname "$0")/variables"
counter=0
for BRANCH in $BRANCHES; do
	echo -e "Checking out files from $BLUE$BRANCH$ENDCOLOR"
	$GIT archive --remote "$REPO" --prefix="$BRANCH-temp/" --format=tar "$BRANCH:po" | tar x
	mkdir "$LCODE-temp"
	counter=0
	for file in $(find "$BRANCH" -follow -name "*.$LCODE.po"); do
		bfile=$(basename "$file")
		echo -n "Processing catalog $bfile... ";
		d=${bfile//".$LCODE.po"/}
		dirdomain=$(echo "wesnoth-$d" | sed "s/wesnoth-wesnoth/wesnoth/")
		if [ -f "$BRANCH-temp/$dirdomain/$LCODE.po" ]; then
			if ! $DIFF -q "$file" "$BRANCH-temp/$dirdomain/$LCODE.po" > /dev/null ; then
				mkdir "$LCODE-temp/$dirdomain"
				cp "$file" "$LCODE-temp/$dirdomain/$LCODE.po"
				echo -e "${GREEN}OK$ENDCOLOR"
				counter=$((counter+1))
			else
				echo -e "${BLUE}Skipping$ENDCOLOR"
			fi
		else
			mkdir -p "$LCODE-temp/$dirdomain"
			cp "$file" "$LCODE-temp/$dirdomain/$LCODE.po"
			echo -e "${GREEN}OK$ENDCOLOR"
			counter=$((counter+1))
		fi
	done

	if [ -f "$LCODE-$BRANCH.tar.bz2" ]; then
	        echo "Removing old archive."
	        rm -f "$LCODE-$BRANCH.tar.bz2"
	fi

	if [ ! $counter -eq 0 ]; then
		cd "$LCODE-temp" || exit
		$TAR --remove-files -cf "../$LCODE-$BRANCH.tar" wesnoth*
		cd .. || exit
		$BZIP -9 "$LCODE-$BRANCH.tar"
		echo "New archive created."
	fi
	rm -rf "$BRANCH-temp"
	rmdir "$LCODE-temp"
done

echo -e "${GREEN}Finished.$ENDCOLOR"
