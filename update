#!/bin/bash
# Update translations from subversion repository and merge them
# Copyright (C) 2007-2008  Andrius Štikonas <andrius@stikonas.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

source "$(dirname "$0")/variables"

echo -e "Language: ${RED}${LCODE}${ENDCOLOR}"
for BRANCH in $BRANCHES; do
    echo -e "Checking out files from ${BLUE}${BRANCH}${ENDCOLOR}"
    if [ ! -e "$BRANCH" ]; then
        CURRENT_DIR="$PWD"
        (
            cd $LOCAL_REPO
            git worktree add "${CURRENT_DIR}/${BRANCH}" "$BRANCH"
        )
    fi
    git archive --remote "$REPO" --prefix="${BRANCH}-temp/" --format=tar "${BRANCH}:po" | tar x
    for dir in $(ls "${BRANCH}-temp/wesnoth"* -d); do
        echo "Processing $(basename "$dir" | sed s/wesnoth-//).${LCODE}.po"
        file="${BRANCH}/$(basename "$dir" | sed s/wesnoth-//).${LCODE}.po"
        if [ -f "${dir}/${LCODE}.po" ]; then
            if [ -f "$file" ]; then
                echo -e "$BLUE""Merging:$ENDCOLOR"
                msgmerge -U -v "$file" "${dir}/$(basename "$dir").pot"
                rm -f "$file~"
            else
                echo -e "${RED}Copying:${ENDCOLOR}"
                cp "${dir}/${LCODE}.po" "$file"
            fi
        else
            if [ ! -f "$file" ]; then
                echo -e "${RED}No po file in repository. Running msginit.${ENDCOLOR}"
                echo "$dir"
                msginit -i "$dir/$(basename "$dir").pot" -o "$file"
            else
                echo -e "${RED}No po file in repository. Found file in workspace.${ENDCOLOR}"
            fi
        fi
        echo ""
    done
    rm -rf "${BRANCH}-temp"
done
echo -e "${GREEN}Finished.${ENDCOLOR}"
